在使用燈及水的時候從身體裡流瀉而出的東西，即魔力。不使用魔法，將手指按在魔晶上讓魔力流入進去就可以。在接觸到魔晶凸起的尖端時將想要向外跑掉的魔力壓回去，與之相對的要將自己的魔力塞入魔晶的內部。這就是給魔晶進行補魔。

要是想要將詳細的東西都寫出來會很複雜，就結果而說麻理子為魔晶補魔並不是什麼困難的事情。麻理子仔細的再次按住一個新的魔晶，總算是把握住了魔晶的魔力壓和容量大小。以那種感覺為基礎，塔莉婭又擺出了其他幾個不同型號的魔晶，讓麻理子完全適應了這份工作。

「嚇了一跳呢。本來想著只要是有魔力的人大都能做到所以你應該也可以，這樣就已經掌握了嗎。而且那個速度。你真的太不可思議了」

看著不斷進行魔力補給的麻理子，塔莉婭發出了驚訝的聲音。

「誒都 ，多摸一摸的話能大致感覺出來可以塞進去多少，接著就是跟著那個感覺塞進去就行」

（多半，是什麼常駐型技能發動了效果的緣故吧。能夠進行這種理解的技能，是魔法理論那一方面的麼）

麻理子一邊回答著塔莉婭一邊進行著思考。在遊戲中的魔法理論是指有關魔法方面的知識和理解，並以此名義進行狀態提升的技能。等級上升能夠提高知識和理解的水平，實際上的效果就是提升遊戲中的MP和智力屬性

是使用魔法的角色能夠取得的最基本的能力之一。當然，【麻理子】也取得了，【麻理子】的魔法理論同恢復性系技能一併升到了最高水平的二十等。想必在這裡實際上是【知識和理解】在發揮作用吧。

「嘛，有那種感覺的話，火矢的控制那種程度應該很快就能做到了吧」

「哎？　啊，確實是那樣」

塔莉婭提起了今天早上的事，麻理子稍作思考的同意了。以火矢為首的遊戲中的魔法，按照這邊的規則來行使是有可能的

（只是，得在我周圍不會受到波及的地方來練習魔法啊）

什麼時候去試試呢，麻理子一邊繼續進行著魔晶補給一邊考慮著。

「那麼，魔力補給這件事，之後就是你的一份工作了」

「是工作，嗎」

「啊。不過，不只是你，只要是住在這裡的人都有著進行魔力補給的工作。在這裡、探險者(エクスプローラー)之証裡的魔力意義完全不一樣」

那樣說著，塔莉婭又將自己的證從架子上拿了下來

「魔晶的魔力減少的話會變白這件事已經明白了吧？　因此，希望你只要看到了這裡的魔力減少就進行補給。喏，從這裡就能看得到魔晶吧？」

用手指著證的台座上。麻理子再次看過去，在台座木雕的部分能看到一個小洞在那裡，從那裡能看到一份位於後方的魔晶。如果沒有這麼一提，那個看上去只像是嵌入其中的水晶裝飾品一樣的東西

「平時主要是我來看護。用不著每天都過來，當我不在的時候過來這個房間注意一下。這邊也有預備用魔晶，要是看到有減少的魔晶，麻煩你也幫忙補給一下」

將自己的證放回架子上的同時，塔莉婭指著放在旁邊的一個小箱子上說道

「知道了」

由於中斷魔力運行是不行的。麻理子老老實實地點了點頭。

「啊，我忘記說了一件很重要的事」

「怎麼了？」

麻理子反問道，塔莉婭變換了變輕重新看先麻理子。

「如果，發現了誰的證出現了裂痕，不管發生什麼都要過來告訴我。明白嗎，這是最優先的，其他的不管什麼都要放在一邊」

「那個……」

證出現了裂痕，意味著那個人的死。麻理子思考著在探險的途中出現了死者意味著什麼

「啊，就是那麼回事。那種情況，大多意味著和那個人同隊的傢伙們都陷入了危險的狀況。那個時候要馬上派出搜查隊。村子附近的村子，某種程度上也能拜託給那個時候在附近的其他小隊。根據場合這裡也會交給薩妮婭由我出馬。已經死掉的人再怎麼樣也沒有辦法，但是其他人或許還來得及」


麻理子聽了剛剛的話，老老實實的點著頭。

「在那發生之前不必放在心上，現在先記好就行。現在這四周，那種事還很少會發生。儘管如此在前方會發生什麼依舊無法知曉的才是人生吶」

「哈……是」

對著表情凝重的麻理子，塔莉婭以一種緩和氣氛的表情說道，麻理子松了口起卸掉了肩膀上的力量。
