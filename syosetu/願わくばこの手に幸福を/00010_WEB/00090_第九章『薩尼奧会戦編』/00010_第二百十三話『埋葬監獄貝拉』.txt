格萊斯特王國東南部，有一個被賦予了埋葬之名的監獄。名叫埋葬監獄貝拉。

如字面所述，被收容在那裡的東西活著的時候暫且不論，就算變成屍體也不能從裡面出來。進入那個監獄的話，等待在前方的命運就只有最後變成骨灰死去。
那個建築本來的作用不是怨嘆回響的監獄，而不過是俘虜收容所。據說是被稱為建築王的先王設置的與前線之間的中繼堡壘。

分隔開首都與前線，覆蓋監獄周圍的水溝與監獄的構造相適應，現在的王登基之後將這個監獄改造成現在的樣子。

這裡的監獄，為了決不讓囚犯逃跑，而布滿了監視塔。出乎意料的是，在這地方被關押的都不是盜賊團的首領和後街的掌權者。倒不如說其中有很多囚犯沒有殺一個人。不管怎麼說，像殺人和盜竊犯這樣的犯罪者，只會被那些無法用語言形容的體力勞動所收納。

對這裡的囚犯進行的並不是體力勞動這種懲罰，而是故意傷害他們、致殘他們的行為，即所謂的為了讓人吐出情報的拷問。也就是說在這裡被關押的都是被稱為思想犯、異端者的人。

舉個例子：希望顛覆國家的反叛者，信仰異教的愚者。迷惑人的術者……雖然沒完沒了，但這些人最麻煩的地方是，就算一個人完蛋了，但還是有人繼承了他們的思想，就像思想是一種財產一樣，不斷地在繼承。

因此，施政者認為切斷這一切的機制是必要的。智者說，如果不像這樣從他們口中探出同伴的名字，把芋頭從地中撈上來那樣連根拔起的話，國家就不會和平。

埋葬監獄貝拉。這裡就是為了那個而存在的，為了讓思想犯，異端者們開口，然後不能再踏上外部的土地。

今天又有誰在監獄貝拉喊破了喉嚨，用血代替聲音溢出。這就這個監獄的日常。
在那個監獄中，當然也有工作的人，看守們。在食堂一邊吃著貧窮的飯菜，看守中的一人用非常焦急的語氣說出了話。周圍用冷石砌成的牆壁遲鈍地反射著他的話語。
“——開玩笑吧，那個女人到底是誰啊。”
不管怎麼說，他的語氣也掩蓋不住其本身的粗暴、粗野。
這個埋葬監獄貝拉旗下的看守，有兩種人。一種是國家信奉者，對國家絕對忠誠，渴望著親自割開反叛國家，仇恨國家者的脖子。

還有一種，則是些僅僅只為了炫耀自己的本領，就穿著看守制服的粗魯的人們。雖然當了看守是件好事，但是由於他們那暴力且不認真的態度，如果是在普通的監獄裡工作的話，他們自己就會被人像驅除麻煩一樣關進監獄。

這樣看來，將此不滿從口中散發出來的男人似乎是後者。

在男人的周圍，聚集著類似的看守們，傾聽著那些從嘴裡說出的話。在娛樂不多的這種地方，看守們最喜歡的娛樂就是抱女人了，發泄不滿和不幸的話語很容易吸引人們接近。就像吸附在傷口的蛭一般。

男人一邊愉快地接受來自周圍的視線，一邊吐出自己的郁憤。
“2066啊。那個女人為什麼會得到那樣的自由？”
男人用這一切毫無道理的樣子說道。對此，周圍的幾個看守都點頭表示同意，不明白的人則詢問旁人到底是怎麼回事。

據說，2066號，被用那個號碼稱呼的囚犯在這個監獄裡實在是不可思議的事，雖然是囚犯的身份，但是卻在這個埋葬監獄貝拉中被允許著自由行動。住著簡直像貴賓室一樣的單間，不是監禁而是軟禁狀態。如果是在監獄中的話，即使是某種程度的出門也可以獲得許可。而且看守長對此也什麼也沒說，閉口不言。真沒想到，那個囚犯甚至居然還跟著類似護衛的人物。

看守的男人說，犯人應該得到那樣的自由嗎？被收監在這裡的人類都是動搖國家，想傷害國王陛下的御身的不敬者們。他誇張地說，對於這樣的人，連呼吸的自由都不應該被允許吧。

周圍的看守們，隱約開始明白這個男人為何突然開始說那樣的不滿。

關鍵是這個男人，想把手伸向那個女囚犯的身體，即使被說越權瀆職也想做吧。就是因為無法實現，所以才在這裡發出無聊的抱怨。
但是，看守們能理解男人的心情。不管怎麼說，在埋葬監獄貝拉中，所謂犯人是與看守可以自由使用的道具相近的存在。

當然也有一定的規則，如果做得太過分了也會被處罰。儘管如此，如果只是‘用’那些囚犯的身體這種程度，壓根就沒受到過責備。特別是對於最近經常被收監的紋章教徒們，對待他們時監獄更是對看守們採取放任般的態度。

儘管處於這種狀態，但如果突然出現了意想不到的囚犯，就很容易想像這些已然習慣這種境遇的看守們心中的焦慮會膨脹起來。
而且傳言中的2066有著奇妙尖銳，很有吸引力的姿容。正因為如此，男人的憤慨更容易理解了。

怎麼樣，那今晚大家一起潛入房間吧。那樣的話題開始有人討論的時候，不知道是誰說：
“你們不知道嗎？如果對那個女人出手，弄不好的話我們就會成為囚犯。”
周圍的人都緊緊地盯著說話的人的眼睛，側耳傾聽。大家都把視線投向了發出聲音的看守，好像在追問他到底在說什麼似的。看守苦澀地歪曲著嘴唇，壓低聲音說：
——那個女人是大罪人路易斯，和聖女阿琉珥娜的養母。

◇◆◇◆

囚犯號碼2066。那是目前被關押在監獄貝拉的孤兒院的管理者南因絲的稱呼。

我倒是並沒有覺得那個稱呼不好。倒不如說到現在為止自己被叫了許多的名字，如果考慮這不過是其中的一個的話並不是那麼壞的。
倒不如說讓人覺得不舒服的是自己的待遇吧。

當南因絲坐在身邊的床上時，那種柔軟的感覺讓人無法想像是為囚犯所準備的。單間也絕對不能說是狹窄，比一般的旅館大得多。而且門上沒有鎖，從監獄出去應該不容易，但多少可以自由地到處走動吧。

在被稱作埋葬監獄的貝拉中，這是難以置信的厚待。這簡直就是對貴族囚犯的待遇。
這究竟是為什麼會有這個待遇，南因絲大概猜得到。儘管如此，令人毛骨悚然的東西還是讓人毛骨悚然。

過去自己養育，然後送到大教堂去的，阿琉珥娜。

因為那孩子走在大聖教的聖女的道路上，自己才受到這樣的待遇吧。如果把南因絲作為異端者處刑，而聖女事後責備了這種行為的話，負責人即使被眾人指責也不奇怪。那是信奉大聖教的人比什麼都害怕的，從神的救濟中被除名的事。正因為如此，南因絲現在受到的待遇也能理解。

同時，自己被監獄貝拉收監的事，也是因為另一個養育的孩子，南因絲很輕地用指尖卷了紫色的頭髮。

——路易斯，那個小鬼，好像很好地成長了。

大罪人。背徳者，然後，被賦予了黃金的紋章名。那些從眼前浮現的過去那孩子貧弱的長相來看，根本就是難以想像的。但是看守長仔細地告訴我，這是千真萬確的事實。

身為大罪人路易斯的養母。這就是南因絲被埋葬監獄貝拉收監的理由。
大概，自己作為紋章教徒的事還沒曝光。南因絲弄濕嘴唇。雖然可能有人對此有懷疑，但還是沒有確信。

如果那個事情被揭發了，那麼即使是聖女的養母，那麼也不會再得到現在的待遇了。不管是會受到貴人專用的拷問，還是就這麼食物中毒莫名其妙地死去，應該都是很有可能的。
雖說現在身處監獄之中，但自由卻得到了寬恕，因此自己至今還充滿著疑惑。大聖教聖女和大罪人，兩個孩子都在搖動著自己的天平。

是啊，現在雖然快要崩潰，但還是實現了平衡的脆弱的天平。因為這種天平在監獄中受到了厚待，產生了扭曲的現狀。
——雖然沒有想像中的那麼壞，但還是很難說好。
由於現在自己的狀況而不知不覺地吐出了氣息，南因絲嘴唇緊繃起來。被抓住也是沒辦法的。倒不如說是為了活用其他東西才應該這樣做的，對此一點也不後悔。鑑於情況來看，就此暗中消失的姿態也挺好。

但是，儘管如此，失去的東西還是很多。
總之，如果在王國中潛藏的紋章教徒們失去了作為主軸而行動的自己的話，向聖女瑪蒂婭和安傳達情報就不容易了，甚至可能連僅僅一點點的支援也不能進行。本來，在王國周邊，狩獵紋章教徒的行動已經愈演愈烈。

南因絲一瞬間皺起眉頭，儘管如此馬上調整了表情。
畢竟在這監獄裡是不能採取太顯眼的行動的。即使寄出一封信也不容易吧。據說紋章教派遣了軍隊，但之後的情報一個也沒有得到。只有做不到的事，不明白的事。冷漠的焦躁，舔著南因絲的胸口。

怎麼辦，怎麼辦。

顯示出聰慧的雙眸，搖搖晃晃地改變了形狀。腦海中浮現出幾種思考，但每次都會消失。無論哪個，都是些不容易實現的方案。如果有安在的話，情況會稍微不同吧。
在腦海中持續浮現著這樣的想法的時候，南因絲的表情突然變化了。想到的事情很奇怪，像是忍不住似的笑聲從嘴唇中流露出來。

愚蠢的想法。對那個總是胡鬧，被阿琉珥娜訓斥的小子抱有期待，開玩笑也有限度。
——但是，沒關係。現在也無法勉強行動。那麼就盡可能地期待英雄殿下一下吧。
在南因絲的眼皮背後，曾經在小道上撿到的孩子的身影清晰地浮現出來。