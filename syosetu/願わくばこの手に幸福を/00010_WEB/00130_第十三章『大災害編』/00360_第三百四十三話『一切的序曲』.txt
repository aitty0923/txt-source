格萊斯特王國最北端，蘇茲菲夫堡。
失去原本的主人的這個城堡，現在感到異常的寂寞。在寒風呼嘯中，只能聽到士兵們粗暴的吐息和吞咽唾沫的聲音。

在外壁上部。走在堆積著死雪的岩板上，多蕾正在說話。可能是因為乾燥和寒氣的原因，她的聲音有些痙攣。
“……將軍，援軍到了。國軍和貴族私兵聯合起來一共六萬左右。其中有騎兵和步兵，還混有一部分的魔法師。”

多蕾本來是瓦蕾麗·布萊特尼斯的副官,不過，她不在的現在，多蕾被任命為這個城堡的留守。因此，現在她支撐著臨時的主人，守御著這座城堡。與作為裝飾的將軍比較，稱她為實質的執政者也不過分。

被安排了作為裝飾的將軍的職位的男人，對此什麼也沒說。是認為那個很輕鬆，還是歸根結底認為這只是一次聯繫任務？多蕾不太明白，只知道他是個非常沉默的人。

那樣的男人，只有在今天這個時候開口說話了：
“這對士兵們來說是不幸的，這樣說比較好吧。還是應該去鼓勵一下他們呢？”
那是軟弱的聲音。即使再怎麼勉強也不會認為那聲音屬於加萊斯特王國的將軍。不管怎麼說，他選擇的都是些只能讓人往文官方面聯想的用詞。

那也是理所當然的，據多蕾的調查，他本來就是立志走政治道路的人。
想來其思考的方法也不是武官式的。外表給人的印象也是溫柔的男子。到底為什麼他要走上武官和將軍的道路，多蕾對此沒有調查的想法。
反正應該是因為家裡的原因，調查的話八成會出現這樣的背景吧。一般來說，貴族就是這樣的。

但是，此時，多蕾並沒有想要貶低站在旁邊的軟弱的溫柔男子，或是侮辱他的想法。不，倒不如說，她對他浮現出了近乎尊敬的念頭。
理由只有一個。因為他並沒有從這裡逃出去。

多蕾把視線投向了像是在死雪的縫隙中看到的那個。
——這最初被認為是多餘的巨大樹木，讓人覺得像是一片無盡的遺跡。
抬起視線，再仰望，抬頭仰望天空，終於能看到頂點的，那麼巨大的建築物。

常見的爬山虎纏繞著，各處都有著苔蘚，腐朽的地方到處可見，可以想像那是已經過了相當長的歲月吧。與其說那是人類的住所，不如說更接近精靈的住所吧。
不管怎樣，那都是巨大的，至少從城堡的外壁看也還必須仰視的那樣巨大。現在它正在一點點地向這邊爬行過來。

是的，越來越近了。巨大的那個東西。
最初大家都說看錯了，是夢幻之類的東西。接著又有傳言說那是魔獸使用的幻術和魔術。
但是，不知何時。誰也不說那種話了。即使不說，也能明白。
從肌膚上感覺到的不協調感，從映入眼簾的實像，然後從耳邊殘留的爬行的聲音，都能告訴你，那是不折不扣的現實。

事實上，那個強大的異物存在著，向著這邊爬來。
——要塞巨獸澤布利斯。
在曾經的時候，後來被人們稱為大災害的象徵的東西。是將森林，建築物，野獸，人，一切都踏破的災禍。

只會吞噬，只會破壞的巨獸，此時還未被這麼命名的它逼近著蘇茲菲夫堡，帶著周圍各種各樣的魔獸群。
到底，軍隊這種只是將人聚集在一起的脆弱組織能夠強行壓制住哪個嗎？向對方舉起槍的時候，不會如同枯葉一般被吹跑而導致結束嗎？

那個巨大，讓看的人不僅有這樣的想像。大魔、魔人。或者說是其他的同級別的什麼呢？那個現在已經到了看得清楚的地方。

被派遣至此的國軍，以及貴族私兵肯定是極其不幸的。多蕾這樣確信，他們已經沒有可以戰鬥的對手了。只是，有需要用那個身體去防御的災害。
如何應對災害？只能忍耐，只能等待一切過去。

如果說有能夠對抗災害的存在，那麼應該是同樣作為災害的自己的主人吧。不不，說到那種程度的話，會被主人罵的吧。多蕾不由得露出苦笑。

死雪纏繞著多雷的頭髮，拍打著她的臉頰。
“……身體發冷啊。進去吧，將軍。還必須指揮國軍呢。”
被稱作將軍的男人，因為多蕾的話而睜大眼睛，明明都這個時候了，感覺還很迷茫的樣子。
“我可以嗎，多蕾？我能指揮所有的士兵？”

當然了。這裡的將軍只有他一人。因為到現在為止只是跟魔獸群的小對抗，隊長級別也可以應對，沒有問題。但是如果率領的是大軍的話，那個指揮應該就是將軍級的。

面對反覆詢問的男子，多蕾抑制住自己呼出白色的嘆息，點頭了。本來想哀嘆這樣的男人當將軍沒問題吧。

如果在那個的面前的話，的確有可能會異樣的突然改變心情的。反正除了自己的主人以外任何人誰都不行。那樣的話，只是做到不逃避，這個男的就可以了吧。在這一點上可以表示敬意。



也許只是個愚蠢的人，和自己一樣。多蕾的嘴唇尖銳地張開。
“需要我代勞嗎？或者委託其他掌管兵權的人，或者……”
是啊，多蕾剛要繼續的時候，像要吞噬住話語一樣，男人反駁。
“——不，好啊。幹吧。我雖然做得很糟糕，但是將軍畢竟是將軍，必須負起責任，也有必要履行作為貴族的義務。”

是高貴者的義務嗎？果然還是不喜歡這個男人。雖然溫柔男本身就不是喜歡的類型。

作為將領的尊敬，只獻給自己的主人。正因為如此，對這個男子的尊敬是對人類的敬意。
雖說平日裡擁有高貴的權利，但是沒有履行義務的貴族到處都是。不用說，那樣的人才可以說是貴族，他們只貪圖權利，不履行義務。
但是這個男人卻很少見，想盡自己的義務。
就這點來說，他十分值得尊敬。即使結果是丟了性命。

“是啊，我的話是四天。弱小的我也能爭取四天給你看。就算魔獸們從外壁上撲上來，我們也要拿著槍戰鬥。所以，多蕾。”

他——將軍眯著眼睛，一邊翻著披在肩上的外套，一邊朝著多蕾張開嘴。果然那張臉無論到哪裡都很溫柔。
多蕾見過各種各樣的將軍和軍人，但是是第一次遇到像他那樣的將領。怎麼想他那樣的表情都不適合戰場。

這樣的他現在，眼中浮現了作為將軍應該擁有的東西，說話了。
“授予你特命，一定要成功。為了國家。”
只說了這些，硬是把卷成小小的圓柱形的羊皮紙收進多蕾的手裡。多蕾認真地聽著他的話，或許這是最後一次了。

他回到了像往常一樣沉默寡言的狀態，然後奔赴戰場。

——六天後。由於要塞巨獸澤布利斯的存在，蘇茲菲夫堡陷落。

沒有發生任何的慈悲和奇跡，守軍全員理所當然地都死了。除了一部分預備兵力以外，城堡內的指揮官和士兵全滅了。為了保護國內的民眾，甚至連記錄他們勇敢戰鬥的書籍都沒有留下來。

因為，沒有一個目擊者。

今後，大災害將拉開厚實的帷幕。