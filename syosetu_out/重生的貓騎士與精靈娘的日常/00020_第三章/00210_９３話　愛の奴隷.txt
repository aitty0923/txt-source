「完，完全打不中啊⋯⋯」
「我不甘心⋯⋯」

一段時間後──

宅邸的庭院裡艾麗婭和史黛拉在蹲著發獃。
渾身汗流浹背，看樣子已經精疲力竭了。


從那之後二人進行了無數次的模擬戰。但是，朱利烏斯皇子徒手便防住了史黛拉的全部攻擊，而以艾莉莎為對手的艾麗婭，每次都以為能讓艾莉莎防禦時卻全被她躲開了。


「你們兩個，不要這麼泄氣。」
「就是喲，況且在這麼點時間裡動作變得相當不錯了。這正是你們成長空間很大的證明。」

朱利烏斯皇子和艾莉莎對泄氣的二人誇讚道。
經過了一段時間的模擬戰，即使是朱利烏斯皇子也流汗了。

但是⋯⋯該怎麼說呢，艾莉莎一滴汗也沒出。而且漂亮的白金色頭髮絲毫不凌亂，長長的女僕裝也沒有沾染一絲塵埃⋯⋯⋯


「唔誒〜，頭好痛〜！」
「學習太難了〜！」

庭院一邊的莉莉和菲莉發著牢騷。
二人在貝露澤碧尤特那以聽講座的方式學習魔法技能的高效發動方法。


對於一直在森林型迷宮裡自由自在生活的二人來說，聽講座實在是一件痛苦的事。
但是，二人再怎麼抱怨，卻還是在認真聽貝露澤碧尤特講話，讓人明白她們內心是真的想幫助艾麗婭等人。

「那麼，今天的訓練到此為止──嗯？這個腳步聲是⋯⋯」

正想說『今天的訓練到此為止』的艾莉莎的耳朵上下抖動了一下。在這時候來的人是⋯⋯⋯

「喵！艾莉莎醬真的在啊喵！許久不見喵！」
「果然是薇兒卡小姐！久疏問候！」

薇兒卡走進了庭院。一看到艾莉莎後就跑過去和她擁抱在一起。
在兩人之間，被完全包裹的薇兒卡的胸部和被女僕裝包裹的艾莉莎的胸部柔軟地擠變了形。

薇兒卡的胸部已經相當的大了，然而艾莉莎還在其之上。

從型號來說和艾麗婭相等⋯⋯不，判斷比艾麗婭更大。
西瓜⋯⋯還達不到，和甜瓜差不多吧──毫無疑問是爆乳一類。


「薇兒卡小姐，和艾莉莎大人那麼親熱⋯⋯兩人相識的事果然是真的呢！」

看到艾莉莎和薇兒卡抱在一起，艾麗婭思索道。

雖然聽說過這事，但是親眼看到自己的同伴薇兒卡和憧憬的剣聖親熱的樣子有這反應也是理所當然。


「當然喵！艾莉莎不只是一起戰鬥的夥伴而且還是重要的朋友喵！」
「呵呵，說的是啊薇兒卡小姐。啊⋯⋯總覺得有點懷念啊，主人剛收留我的時候，那一天去薇兒卡小姐的店裡購買武器⋯⋯」
「是的喵⋯⋯那時的舞夜桑才來這個世界，連杖的使用方法都不知道喵。」

看著靜靜地回憶往事的艾莉莎和薇兒卡。似乎艾麗婭內心已經將二人的關係上升到具有強大牽絆的地步了。

而且沒想到以前聽薇兒卡說她教過大魔導士──舞夜關於杖的使用方法這事居然是真的，艾麗婭深受感動。


⋯⋯但是，艾麗婭更加在意的是這之後艾莉莎所說的話。

（被主人收留⋯⋯？究竟是指什麼呢）

──正想著⋯⋯⋯


不知當問不當問⋯⋯⋯帶著一堆疑問，艾麗婭把這些相關的問題都丟給了艾莉莎。
於是乎，從她那聽到了衝擊性的發言。

「呵呵⋯⋯我是主人的妻子的同時也是奴隷。」
「奴，奴隷！？作為剣聖的艾莉莎大人，是大魔導士的⋯⋯！？」
「沒錯，艾麗婭醬。啊⋯⋯一回想起那天的事。主人那天把被奴隷商人所騙導致成為奴隷而絶望的我救了出來⋯⋯♡。」

艾莉莎回答了驚得不知所措的艾麗婭後，又想到了過去的事，臉上染上了一抹粉紅。
也不知眼睛是何構造，艾莉莎眼睛裡冒出小小的粉心，女僕裝下的大腿也扭扭捏捏地不斷摩擦。

光是站在那就覺得美麗，不斷散發魅力的年長女性又做出那種事⋯⋯連同性的艾麗婭看到也覺得過於妖艷，因此難免會產生奇怪的感情。

（話說⋯⋯剣聖艾莉莎的首飾，仔細看的話與其說是項鏈，不如說是奴隷項圈更合適一點。她是大魔導士的夫人的同時，也是奴隷這事似乎是真的）

艾麗婭的身旁，塔瑪抬頭看了看艾莉莎想著。

艾莉莎的首飾是合適上好的白銀色的物品。中央鑲嵌著與艾莉莎瞳孔顏色相同的冰藍色的寶石，散發出璀璨的光芒。

咋一看就是貴重的首飾，然而這形狀⋯⋯說不像奴隷的項圈呢也不太對。

（被從絶望中救出的話語，再加上艾莉莎大人幸福的表情⋯⋯⋯作為大魔導士大人的奴隷這一立場，對自己的處境感到自豪，還感到幸福的嗎。雖然不太清楚緣由但是再問的話就是不解風情了⋯⋯）

從她的表情裡，艾麗婭確信了她很幸福。並且意識到再問下去的話就很失禮了。

⋯⋯嘛，對艾莉莎來說，與所愛的主人相遇，何時開始戀愛等等，關於這些提問的話想必是非常歡迎的──這些暫且不提。


════════════════════

【出版通知】

承蒙您的關照。

姐妹作品《在地球上被虐待的「最強」暗魔術士，在異世界被妖精兒媳婦所愛》為基礎的輕小說出版決定了。

書籍版標題是『被虐待了的『最強』魔導士在異世界被妖精兒媳婦愛』

發售預定在二月，正好現在，在本作品貝西蒙斯裡（上）登場了的「艾莉莎」是主要女主人公的話。

能像這樣把首次投稿的作品變成書籍，多虧了愛讀的各位。真的非常感謝。
關於詳細情況，我想在今後的活動報告和後記中告知，請多關照。