---
LocalesID: 重生的貓騎士與精靈娘的日常
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8.ts
---
__TOC__

[重生的貓騎士與精靈娘的日常](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8.ts)  
總數：97／98

# Pattern

## 艾麗婭

### patterns

- `アリア`
- `亞利亞`
- `艾利亞`
- `亞里亞`
- `阿利亞`
- `艾莉婭`
- `阿莉亞`
- `雅利安`
- `阿麗亞`
- `艾莉亞`

## 塔瑪

### patterns

- `タマ`
- `塔摩`
- `Tama`

## 史黛拉

### patterns

- `ステラ`
- `斯黛拉`

## 庫斯曼

### patterns

- `カスマン`
- `卡斯曼`

## 安娜

### patterns

- `アナ`

## 阿納爾德

### patterns

- `アーナルド`
- `阿納多`
- `安娜路德`
- `阿爾納德`

## 霍茲尼格爾

### patterns

- `ホズィルズネッガー`

## 薇兒卡斯

### patterns

- `ヴァルカンズ`

## 薇兒卡

### patterns

- `ヴァルカン`
- `伏爾坎`
- `伏爾甘`
- `維爾干`
- `伏爾干`
- `維爾幹`
- `薇爾卡`
- `瓦爾肯`

## 櫻

### patterns

- `サクラ`

## 萊納德

### patterns

- `レナード`
- `雷納德`

## 塞德里克

### patterns

- `セドリック`
- `塞德里克`
- `塞德克`
- `杰特里克`
- `賽德利克`

## 李文

### patterns

- `リューイン`

## 丹尼

### patterns

- `ダニー`
- `達尼`
- `Danny`

## 霍華德

### patterns

- `ハワード`

## 凱妮

### patterns

- `ケニー`
- `肯尼`
- `凱尼`

## 瑪麗埃塔

### patterns

- `マリエッタ`
- `瑪麗塔`

## 貝利爾

### patterns

- `ベリル`
- `貝利爾`
- `貝利魯`
- `利貝魯`
- `貝里魯`

## 阿斯塔洛斯

### patterns

- `アスタロス`

## 雷伊斯

### patterns

- `レイス`

## 格拉德斯通

### patterns

- `グラッドストーン`

## 莉莉

### patterns

- `リリ`

## 菲莉

### patterns

- `フェリ`

## 喬伊

### patterns

- `ジョーイ`

## 艾莉莎

### patterns

- `アリーシャ`

## 奧修拉

### patterns

- `アウシューラ`

## 克拉利亞爾

### patterns

- `クラリアル`

## 蓋澤魯

### patterns

- `ガイゼル`

## 朱利烏斯

### patterns

- `ジュリウス`

## 貝露澤碧尤特

### patterns

- `ベルゼビュート`

## 瓦沙克

### patterns

- `ヴァサーゴ`

## 瑪門

### patterns

- `マモン`

## 莉維

### patterns

- `レヴィ`

## 魯米納斯

### patterns

- `ルミルス`
- `魯米納斯`
- `路米爾斯`

## 阿爾法斯

### patterns

- `アルフス`

## 菲奧妮

### patterns

- `フィオーネ`

## 雷歐

### patterns

- `レオ`

## 埃魯文

### patterns

- `エルヴン`

## 蕾歐娜

### patterns

- `レオナ`

## 艾菈

### patterns

- `エイラ`

## 地龍

### patterns

- `アースドラゴン`
- `地球龍`

## 龍

### patterns

- `ドラゴン`

## 精靈

### patterns

- `エルフ`

## 貝希摩斯

### patterns

- `ベヒーモス`
- `貝西摩斯`
- `貝爾摩西`
- `貝希摩斯`
- `貝希莫斯`

## 元素貓

### patterns

- `エレメンタルキャット`
- `Elart Catt`

## 米諾陶諾斯

### patterns

- `米諾陶諾斯`
- `米諾陶洛斯`
- `米諾塔洛斯`
- `米諾塔尼洛斯`
- `彌諾陶洛斯`
- `ミノタウロス`

## 哥布林

### patterns

- `ゴブリン`

## 大哥布林

### patterns

- `ホブゴブリン`

## 哥布林法師

### patterns

- `ゴブリンメイジ`
- `哥布林染井`
- `牛郎化妝`

## 狼人

### patterns

- `ワーウルフ`

## 巨魔

### patterns

- `トロール`

## 毒龍

### patterns

- `波茲龍`
- `毒普斯龍`

## 魔物

### patterns

- `モンスター`

## 蜥蜴馬

### patterns

- `リザードキャリア`

## 瑪娜

### patterns

- `マナ`
- `Manna`

## 水・咆哮

### patterns

- `Water - Howling`
- `ウォーター・ハウリング`
- `水#_@_#咆哮`

## 以太・咆哮

### patterns

- `Ether-Howling`
- `エーテルハウリング`
- `空氣─咆哮`

## 技能喰奪

### patterns

- `スキル喰奪`

## 劇毒獠牙

### patterns

- `ポイズンファング`

## 炎刃

### patterns

- `火焰刃`
- `フレイムエッジ`
- `火焰之邊緣`
- `火山口邊緣`

## 空刃

### patterns

- `空氣刃`

## 岩刃

### patterns

- `岩石刃`

## 階級

### patterns

- `ランク`
- `Rank`

## 鋼軀

### patterns

- `鋼鐵身體`
- `アイアンボディ`

## 冰槍

### patterns

- `アイシクルランス`
- `艾西斯斯蘭斯`

## 賦予・烈焰

### patterns

- `エンチャント・フレイム`
- `天使·弗雷姆`

## 火球

### patterns

- `火焰球`
- `ファイアーボール`

## 超加速

### patterns

- `アクセラレーション`

## 魔神的黃昏

### patterns

- `魔神の黃昏`

## 諸神黃昏

### patterns

- `ラグナロク`
- `拉格納洛克`

## 群體守護之壁

### patterns

- `マルチプロテクションウォール`

## 龍之牙

### patterns

- `ドラゴンファング`

## 轉生

### patterns

- `リーンカーネーション`

## Excalibur

### patterns

- `エクスキャリバー`

## 戰錘

### patterns

- `バトルハンマー`
- `戰鬥漢馬`
- `戰鬥錘`

## 巨劍

### patterns

- `グレートソード`

## 巴斯特劍

### patterns

- `バスターソード`

## 真實之雫

### patterns

- `真実の雫`
- `真實之滴`

## 涅磐鋼

### patterns

- `ヴィブラウム`
- `振金`

## 奧利哈魯鋼

### patterns

- `奧利哈剛`
- `奧利哈魯鋼`
- `奧利哈爾鋼`

## 卡拉德波加

### patterns

- `カラドボルグ`

## 光之劍

### patterns

- `クラウソラス`

## 猜疑之戒

### patterns

- `デファイヨンリング`

## 精金

### patterns

- `アダマンタイト`

## 神器

### patterns

- `アーティファクト`

## 喵呀

### patterns

- `んにゃ`

## 呣扭

### patterns

- `むにゅむにゅ`


